\contentsline {chapter}{List of Figures}{vi}{chapter*.2}
\contentsline {chapter}{List of Tables}{viii}{chapter*.3}
\contentsline {chapter}{Acknowledgments}{ix}{chapter*.4}
\contentsline {chapter}{Chapter{} \numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Contributions}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Dissertation Organization}{6}{section.1.2}
\contentsline {chapter}{Chapter{} \numberline {2}The Evolutionary Origins of Hierarchy}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Abstract}{7}{section.2.1}
\contentsline {section}{\numberline {2.2}Introduction}{8}{section.2.2}
\contentsline {section}{\numberline {2.3}Results}{13}{section.2.3}
\contentsline {section}{\numberline {2.4}Discussion}{22}{section.2.4}
\contentsline {section}{\numberline {2.5}Methods}{24}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Experimental setup}{24}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Statistics}{24}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Evolutionary algorithm}{24}{subsection.2.5.3}
\contentsline {subsection}{\numberline {2.5.4}Behavioral diversity}{25}{subsection.2.5.4}
\contentsline {subsection}{\numberline {2.5.5}Connection cost}{26}{subsection.2.5.5}
\contentsline {subsection}{\numberline {2.5.6}Network model and its biological relevance}{26}{subsection.2.5.6}
\contentsline {subsection}{\numberline {2.5.7}Mutations}{28}{subsection.2.5.7}
\contentsline {subsection}{\numberline {2.5.8}Modularity}{28}{subsection.2.5.8}
\contentsline {subsection}{\numberline {2.5.9}Default hierarchy metric}{29}{subsection.2.5.9}
\contentsline {subsection}{\numberline {2.5.10}Alternate hierarchy metric}{30}{subsection.2.5.10}
\contentsline {subsection}{\numberline {2.5.11}Functional hierarchy}{30}{subsection.2.5.11}
\contentsline {chapter}{Chapter{} \numberline {3}Evolvability Search: Directly Selecting for Evolvability in order to Study and Produce It}{32}{chapter.3}
\contentsline {section}{\numberline {3.1}Abstract}{32}{section.3.1}
\contentsline {section}{\numberline {3.2}Introduction}{33}{section.3.2}
\contentsline {section}{\numberline {3.3}Background}{35}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Evolvability in Evolutionary Computation}{35}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}NeuroEvolution of Augmenting Topologies}{37}{subsection.3.3.2}
\contentsline {section}{\numberline {3.4}Evolvability Search}{37}{section.3.4}
\contentsline {section}{\numberline {3.5}Experiments}{39}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Maze Navigation Experiment}{39}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Maze Navigation Experiment}{39}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Biped Locomotion Experiment}{41}{subsection.3.5.3}
\contentsline {section}{\numberline {3.6}Results}{43}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Maze Navigation Results}{43}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Biped Locomotion Results}{44}{subsection.3.6.2}
\contentsline {section}{\numberline {3.7}Evolvability generality test}{45}{section.3.7}
\contentsline {section}{\numberline {3.8}Discussion}{47}{section.3.8}
\contentsline {section}{\numberline {3.9}Conclusion}{51}{section.3.9}
\contentsline {chapter}{Chapter{} \numberline {4}Discussions and Conclusion}{52}{chapter.4}
\contentsline {chapter}{Appendix{} \numberline {A}Supporting Information}{56}{appendix.A}
\contentsline {chapter}{References}{76}{figure.A.21}
