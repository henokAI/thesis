For the simplest setup:

Place all the files from this archive in one directory 
such as "C:\My Documents\diss" or something similar.

All your own info, such as thesis title, your name, 
committee names, etc., are all set in the "uwyo_0.tex" 
file.  Read all the comments in this file!

Rename the "uwyo_0.tex" file to something more 
appropriate for you, such as "Smith_thesis.tex."

All the graphic files should also be in the same 
directory and must be in EPS format.

If you wish to keep all your figures in a separate 
directory, use the LaTeX command \graphicspath{} 
in your version of the "uwyo_0.tex" file.

Except in rare circumstances, you will not need 
to change the "uwyo_thesis.sty" file at all.  

The files such as uwyo_1.tex, uwyo_2.tex, ... are for 
your own chapter content. What is there now is only 
there as an example.  Delete it and write your own 
chapters.

You can now concentrate on the *content* of your 
document, not the formatting.

Enjoy not having to wrestle with MS Word!

--chgw


