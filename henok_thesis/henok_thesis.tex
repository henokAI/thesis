%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This document is part of the set of template files
%% for the uwyo_thesis style. This is your main file that
%% helps compile the entire document; each individual chapter
%% should be a separate file that is brought in using the
%% "\include{}" commands shown below.
%%
%% Step one: rename this file to something specific to you, such as
%% "Smith_thesis.tex" so your eventaul PDF file will end up having the
%% name "Smith_thesis.pdf."  You can also rename any individual chapter
%% files; if you do, be sure to make the appropriate change to the
%% "\include{}" commands below.
%%
%% Important: only *this* file should be compiled by LaTeX.
%% The individual chapter files are *not* standalone files.
%%
%% For interdisciplinary programs such as Neuroscience, see the
%% special section below
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[12pt,oneside,openany]{book}  % single-sided printing
%\documentclass[12pt]{book}                  % double-sided printing
%% First line above assumes single-sided printing.
%% For double-sided (duplex) printing use the second line instead.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{uwyo_thesis} % the main style file

%%% Many common packages are already loaded by the style file
%%% Add any others below
%\usepackage{??}
\usepackage{nameref,hyperref}

%%% Note about your title:
%%% Due to limitations in the UW Banner system, you may want to limit your
%%% thesis/dissertation titles to the following:
%%%  Masters thesis titles:  214 characters including spaces
%%%  Ph.D. dissertation titles: 208 characters including spaces



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% set these values for your particular document and
%%% uncomment the lines below as needed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\mastersthesis % uncomment this if a Masters thesis; comment out if PhD
%\thesisdraft  % uncomment to put time/date/draft stamp in header
%\renewcommand{\thesisauthorLname}{your last name}
%\renewcommand{\thesisauthorFMname}{your first name and middle initial}
%\renewcommand{\thesismonth}{your graduation month}
%\renewcommand{\thesisyear}{your graduation year}
%\renewcommand{\thesisDefenseDate}{the exact day you defend your thesis}
%%% NOTE: you must enter the same title for both \thesistitle and \abstitle below
% Do not use all upper case! That will be done for you where needed.
%\renewcommand{\thesistitle}{your document title}
%\renewcommand{\abstitle}{\ul{your document title}}
% Must be exactly the same as \thesistitle
%\renewcommand{\thesisDeptName}{your academic department or interdisciplnary program}
%\renewcommand{\thesisDeptHead}{name of your academic department head or program chair}
%\renewcommand{\thesisCollegeName}{your college}
%\renewcommand{\thesisDeanName}{name of your college Dean}
%\renewcommand{\thesisDegreeArea}{what your degree is in}
%\renewcommand{\thesisauthorpreviousdegrees}{any of your previous degrees}
%\renewcommand{\lstlistlistingname}{my program listings} % if you want an alternate name for the list
%\renewcommand{\thesisDeptHeadTitle}{Interim Head} % if you're between Dept Heads
%\renewcommand{\thesisDeanTitle}{Interim Dean} % if you're between Deans
%\renewcommand{\thesisdedication}{your dedication} % no length limit, but don't get carried away!

%%%%%%%% Interdisciplinary programs section %%%%%%%%%%%%%%%%%%%
%% You need to define things a bit differently:
%% \renewcommand{\thesisDeptName}{the name of your interdisciplnary program}
%% \renewcommand{\thesisDeptHead}{name of your interdisciplinary program chair}
%% \renewcommand{\thesisDeptHeadTitle}{Program Chair}
%% \renewcommand{\thesisCollegeName}{University of Wyoming}
%% \renewcommand{\thesisDeanName}{name of the UW Provost}
%% \renewcommand{\thesisDeanTitle}{Provost}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% Required: insert your committee member names inside the curly braces
%%% then uncomment the needed lines
% \thesisCommitteeChair{}  % this is your advisor
% \thesisFirstMember{}  % this should be the member *outside* your department
% \thesisSecondMember{}
% \thesisThirdMember{}
% \thesisFourthMember{}
% \thesisFifthMember{}
% \thesisSixthMember{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% Print initial frontmatter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generate the committee page.
%%%  If you want to save a page, you can comment
%%%  this out for early drafts as it isn't needed
%%%  until you're ready for the final version.
\thesisCommitteePage
%%%


%%% the abstract page
\begin{thesisabstract}
~\emph{Evolvability} is an important factor in evolution's ability to create the engineering marvels that are ubiquitous in the natural world. It allows individual species to avoid extinction, invade new niches, and quickly adapt to new environments. For these reasons, producing evolvability in artificial evolution has been a long-standing research goal, both to better understand evolvability from a biological motivation and to harness evolvability in evolutionary algorithms to solve challenging engineering problems. Despite concerted effort by the research community, the conditions that promote evolvability are poorly understood and successful efforts to produce evolvability in artificial evolution are rare and have occurred only in narrowly tailored situations. The assumption by the research community is that we have yet to truly identify the main drivers of evolution in nature and that, as we increase our understanding of the natural drivers of evolution, we will be able to implement them and observe significant increases in evolvability in simulations of evolution and evolutionary algorithms. Doing so will improve our understanding of evolvability and reap tremendous societal benefits in all the various fields that harness evolution via evolutionary algorithms to solve challenging engineering problems.

Although evolvability is important both in natural and artificial evolution, there is no overall agreement on its definition. The two common definitions are (1) evolvability as the ability of populations to quickly adapt to unseen environments, hereafter called \emph{adaptive evolvability}and (2) evolvability as a tendency of an individual's offspring to exhibit phenotypic diversity, hereafter called \emph{phenotypic-variation evolvability}. During my Ph.D. I wrote two papers that present methods of producing both types of evolvability in artificial evolution. 

My first paper shows that the evolution of a hierarchical structure improves adaptive evolvability. Hierarchy is a recursive composition of modules and is a prevalent organizational property in both man-made and natural systems. Before showing how this common organizational structure could increase adaptive evolvability, the paper investigates why hierarchy evolves in the first place. It shows that a cost for network connections promotes the evolution of hierarchy, shedding light on the biological mystery of the evolutionary origins of hierarchy and providing a practical tool for encouraging hierarchy in networks evolved with evolutionary algorithms.  Additional experimental results show that such hierarchy increases adaptive evolvability.
I should note that this paper was a thorough and exhaustive examination of one way of promoting adaptive evolvability (encouraging hierarchy via a connection cost), representing many years of experimentation, analysis, and writing effort. It could have been broken down into smaller publishable units, but I chose instead to present one detailed, experimentally extensive, exhaustive journal paper on the subject. 
%i.e., it identifies the condition under which hierarchy evolves. %Recently, it has been shown that the existence a cost for network connections leads to the emergence of modules. Because hierarchy requires modules, it is reasonable to investigate if the same factor causes the evolution of hierarchy.

My second paper introduces a new search algorithm called \emph{Evolvability Search} that significantly increases phenotypic-variation evolvability by directly selecting for it. The algorithm quantifies phenotypic-variation evolvability by measuring the propensity of phenotypic variation among an organism's offspring and then incorporates that quantification into a fitness function of an evolutionary search algorithm. That is, individuals whose offspring posses high phenotypic variation are rated highly by the fitness function and selected for reproduction. Results from experiments in two evolutionary robotics domains confirm that Evolvability Search produces high-performing solutions with an elevated phenotypic-evolvability. 

Overall, my Ph.D. study produced two papers that introduce methods of enhancing adaptive and phenotypic evolvability, contributing to the long-standing goal of evolving increasingly complex behaviors. In addition to showing methods that increase evolvability, my papers shed light on the fields of robotics and biology. For example, knowing the evolutionary origin of hierarchy will accelerate future research into evolving more complex, intelligent computational brains for robots. \emph{Evolvability Search}, on the other hand, will make generating phenotypic-evolvability easy and straightforward, facilitating our ability to study, understand, and harness evolvability. 

%The first paper shows that adaptability, an important property of natural organisms, increases through the evolution of hierarchy. The second paper presents a novel evolutionary search algorithm that enables population to constantly invent new behaviors, which is a step stone to create complex behavior.   



\end{thesisabstract}

 \thesistitlepage        % print title page
 \thesiscopyrightpage    % print the copyright page
 \thesisdedicationpage   % print the dedication page

% Generate and print the lists
 \tableofcontents        % table of contents
 \listoffigures          % List of Figures
 \listoftables           % List of Tables
 % comment out line below if you have no program listings
 %\mylistoflistings      % List of program listings


%% acknowledgments section: don't forget to thank your committee
%% members, any funding sources/grants, even your Mom and Dad if you want
\begin{thesisacknowledgments}
I acknowledge that rock climbing is awesome, and some beer is, without a doubt, MUCH, MUCH better than other beer. 
\end{thesisacknowledgments}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Now this is where your various chapters come in
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Bring in the chapter files
\include{chapter_one} % chapter 1 Introduction
\include{chapter_two} % chapter 2 EOH paper
\include{chapter_three} % chapter 3 ES paper
\include{chapter_four} % chapter 4 Discussion and Conclusion
%\include{chapter_five} % chapter 5 
%\include{chapter_six} % chapter 6
%\include{chapter_seven} % chapter 6
% add other chapters here

% change formatting for any appendices
% comment this out if you have no appendices

%\appendix
\include{chapter_five} % chapter 5  % Supporting information
%\include{uwyo_B} % appendix B


% Generate the References section using BibTeX. Optionally you
% can code this by hand (argh!)

\addcontentsline{toc}{chapter}{\bibname} % make reference section show up in TOC

\begin{spacing}{1.0} % sets line spacing of references
% reads in the references BibTex file supplied; change to your bib file name
\bibliography{refs}
\bibliographystyle{ieeetr} % IEEE standard citation style
\end{spacing}


%\begin{thesisauthorvita}
%This is where you put your vita if needed. Not usually used at UW.
%\end{thesisauthorvita}


%% all done!
\end{document}
